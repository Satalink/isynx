
package isynx;

import java.awt.Image;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import javax.swing.JFileChooser;

/**
 *
 * @author Satalink
 */
public interface Pkg_Interface {
    public void exitApp();
    public boolean fileCopy (File fromFile, File toFile) throws FileNotFoundException;
    public boolean fileDelete(File file);
    public String filePermisions(File file);
    public File fileRoot(File file);
    public Map getConfigMap();
    public URL getURL(String link);
    public DB getDB();
    public JFileChooser getFileChooser();
    public float getFileSize(File file);    
    public GUI getGUI();
    public Image getGUIIcon();
    public String getHostName();
    public Map getOptionMap();    
    public long getTimeEpoch();
    public long getTimeEpoch(String datetime);
    public String getTimeStamp();
    public String getTimeStamp(long epoch);   
    public String getUserName();
    public boolean hasLicense();
    public boolean synchronizeItem(int mapID, int tableRow);
    public ArrayList updateFileData();
    public void writeLog(String text, int style);
}
