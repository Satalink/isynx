
package isynx;

import java.awt.Color;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DB {
    // runtime variables.
    private Pkg_Interface main;
    private String DBDriver = "org.apache.derby.jdbc.EmbeddedDriver"; 
    private String dbName = "iSynxDB";
    private String dbUsr = "iSynx";
    private String dbPass = dbUsr;
    private String dbProtocol = "jdbc:derby:";
    private String dbURL;
    private String dblogon;
    private String dbcreate;
    private Connection conn;
    private boolean status = false;

    public DB(Pkg_Interface iface) {
        conn = null;
        main = iface;
        dbURL = dbProtocol+dbName;
        dblogon = dbURL+";user="+dbUsr+";password="+dbPass;
        dbcreate = dbURL+";create=true;user="+dbUsr+";password="+dbPass;
    }
    
    public boolean DBexists() {
        return(new File(dbName).exists());
    }
    public void DBclose() {
        try {
            if (conn != null) {
                DriverManager.getConnection(dblogon+";shutdown=true");
                conn.close();
                conn = null;
            }           
        }
        catch (SQLException sqlExcept) {
        }
    }  
    public void DBcompress(String table) {
        try {
            String sql = "SELECT * FROM " + table + " ORDER BY TIMESTAMP";
            ArrayList tabledata = new ArrayList();
            Statement stmt = conn.createStatement();
            ResultSet resultset = stmt.executeQuery(sql);
            ResultSetMetaData rsmd = resultset.getMetaData();
            int columns = rsmd.getColumnCount();
            String[] colNames = new String[columns];
            int[] colTypes = new int[columns];
            int i = 0;
            while (resultset.next()) {
                int col = 1;
                Map resultMap = new HashMap();
                while (col <= columns) {
                    String value = resultset.getString(col);
                    String colName = rsmd.getColumnName(col);
                    resultMap.put(colName, value);
                    if(i==0) {
                        colNames[col-1] = colName;
                        colTypes[col-1] = rsmd.getColumnType(col);
                    }
                    col++;
                }
                tabledata.add(i, resultMap);
                i++;
            }
            sql = "DELETE FROM " + table + " WHERE ID > 0";
            DBstatement(sql);
            for (int r = 0; r < tabledata.size(); r++) {
                Map dataMap = (Map) tabledata.get(r);
                String data = "";
                for (int c = 0; c < dataMap.size(); c++) {
                    if (colNames[c].equals("ID")) continue;
                    if (colTypes[c] == 12 || colTypes[c] == 93) data += "'";
                    data += dataMap.get(colNames[c]).toString().replace("'", "''");
                    if (colTypes[c] == 12 || colTypes[c] == 93) data += "'";                    
                    if (c < (colNames.length - 1)) data += ",";
                }
                sql = "INSERT INTO " + table + " VALUES(" + (r + 1) + "," + data + ")";
                DBstatement(sql);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public boolean DBconnect() {
        status = false;
        if(DBexists()) {    
            try {
                Class.forName(DBDriver).newInstance();
                conn = DriverManager.getConnection(dblogon);
                status = true;
                return(status);
            } catch (Exception ex) {
                return(status);
            }
        } else {
            try {
                main.getGUI().setMsgArea("Creating Database",Color.BLUE.darker(),Color.WHITE,7);
                Class.forName(DBDriver).newInstance();
                conn = DriverManager.getConnection(dbcreate);		
                status = DBreconnect();
                if(status) {
                    main.getGUI().setProgressBarIndeterminate(false);
                    DBinit();
                }
                return(status);
            } catch (Exception ex) {
                return(status);
            }
        }
    }  
    protected void DBinit() {
        String sql;
        int tasks = 2;
        int task = 0;
        main.getGUI().setMsgArea("Initializing Database",Color.BLUE,Color.WHITE,7);
        main.getGUI().setProgressBarMax(tasks);
        // Creation of CONFIGS
        sql = 
            "CREATE TABLE SYNC.CONFIGS ("+
            "ID INTEGER not null primary key,"+
            "GUIPOS_X INTEGER,"+
            "GUIPOS_Y INTEGER,"+
            "GUIDIM_H INTEGER,"+
            "GUIDIM_W INTEGER,"+
            "SYSTEMNAME VARCHAR(256),"+
            "USERNAME VARCHAR(256),"+
            "TABLE_COLS VARCHAR(32),"+
            "CONFIRM_DELETE INTEGER,"+
            "CONFIRM_SYNC INTEGER,"+
            "MINTOTRAY INTEGER,"+
            "ALLWAYSONTOP INTEGER,"+
            "STARTMIN INTEGER,"+
            "FRONTONSYNC INTEGER,"+
            "LOGLINES INTEGER"+
            ")";
        status = DBstatement(sql);
        main.getGUI().setProgressBarValue(task++);
        sql = "INSERT INTO SYNC.CONFIGS VALUES ("+
              "1"+","+
              main.getGUI().getWinCoord().x+","+
              main.getGUI().getWinCoord().y+","+
              main.getGUI().getWinSize().height+","+
              main.getGUI().getWinSize().width+","+
              "'"+main.getHostName()+"',"+
              "'"+main.getUserName()+"',"+
              "'0.0.290.290.60',"+
              "1,"+
              "1,"+
              "1,"+
              "0,"+
              "0,"+
              "0,"+
              "30"+
              ")";
        status = DBstatement(sql);
        main.getGUI().setProgressBarValue(task++);
        // Creation of FILES
        sql = 
           "CREATE TABLE SYNC.FILES ("+
            "ID INTEGER not null primary key,"+
            "PATH VARCHAR(4096),"+
            "FREQUENCY INTEGER,"+
            "SYNCTO VARCHAR(4096),"+
            "TIMESTAMP TIMESTAMP,"+
            "UPDATEON TIMESTAMP,"+
            "USERNAME VARCHAR(128),"+
            "VISIBLE INTEGER)";
        status = DBstatement(sql);
        main.getGUI().setProgressBarValue(task++);
        sql = 
           "CREATE TABLE SYNC.LOG ("+
            "ID INTEGER not null primary key,"+
            "ENTRY VARCHAR(4096),"+
            "STYLE INTEGER,"+
            "USERNAME VARCHAR(128),"+
            "TIMESTAMP TIMESTAMP)";
        status = DBstatement(sql);
        main.getGUI().setProgressBarValue(task++);
        sql = "INSERT INTO SYNC.LOG VALUES ("+
              "1,"+
              "'Database Initialized Successfully',"+
              "1,"+
              "'"+main.getUserName()+"',"+
              "'"+main.getTimeStamp()+"')";
        status = DBstatement(sql);
        main.getGUI().setProgressBarValue(task++);        
    }
    public boolean DBreconnect() {
        if(DBexists()) {
            try {
            status = false;
            Class.forName(DBDriver).newInstance();
            conn = DriverManager.getConnection(dblogon);
            status = true;
            return(status);
            } catch (Exception ex) {
            return(status);
            }
        }
        return(status);
    }
    @SuppressWarnings("unchecked")
    public ArrayList DBselect(String sqlstatement) {
        ArrayList<Map> resultList = new ArrayList<Map>();
        try {
            if(conn == null || conn.isClosed()) {
                DBconnect();
            }
            if(!conn.isClosed()) {
                Statement stmt = conn.createStatement();
                ResultSet resultset = stmt.executeQuery(sqlstatement);
                ResultSetMetaData rsmd = resultset.getMetaData();
                int columns = rsmd.getColumnCount();
                int i = 0;
                while (resultset.next()) {
                    int col = 1;
                    Map resultMap = new HashMap();
                    while (col <= columns) {
                        String value = resultset.getString(col);
                        String colName = rsmd.getColumnName(col);
                        resultMap.put(colName, value);
                        col++;
                    }
                    resultList.add(0, resultMap);
                    i++;
                }
                resultset.close();
                stmt.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return resultList;
    }
    public boolean DBstatement(String sqlstatement) {
        try {
            conn.createStatement().execute(sqlstatement);
            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, sqlstatement, ex);
            status = false;
        }
        return(status);
    }  
}

