
package isynx;

import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;

public class Main implements Pkg_Interface {
    public String PROGNAME = "iSynx";
    public Double VERSION = 0.2;
    public DB db;
    public GUI gui;
    public Splash splash;
    public String appIconRef = "iSynx2.png";
    private ArrayList fileData;
    private License license;
    private File[] roots;
    private String userName;
    private String hostname;
    public  Map configMap;
    public Map optionMap;
    private Map<String,String> rootMap;
    private JFileChooser fc;
    private String sql;
    public Main() {
        try {
            int max = 3;
            int pval = 0;
            splash = new Splash(this);
            splash.setMin(0);
            splash.setMax(max);
            splash.setValue(pval);
            userName = getUserName();
            hostname = getHostName();
            roots = File.listRoots();
            license = new License();
            hasLicense();
            gui = new GUI(this);
            splash.setValue(pval++);
            if(!gui.systemTraySupported()) gui.setDefaultCloseOperation(GUI.EXIT_ON_CLOSE);
            gui.setAllwaysOnTop(true);
            db = new DB(this);
            splash.setValue(pval++);
            Image winicon = new ImageIcon(getURL(appIconRef)).getImage();
            gui.setIconImage(winicon);
            gui.setTitle(PROGNAME+" v"+VERSION);
            optionMap = gui.getOptionMap();
            gui.createLogPane();
            fc = new JFileChooser();
            Thread rootMonitorThread = RootMonitor();
            rootMonitorThread.setPriority(Thread.NORM_PRIORITY - 1);
            rootMonitorThread.start();            
            if (!db.DBconnect()) {
                splash.setValue(pval++);
                gui.setMsgArea("Database connection FAILED!", Color.WHITE, Color.RED,7);
                gui.setLocation(150, 100);
                if(splash != null) splash.killsplash();
                gui.setSplitPane1(gui.getSize().width/4);
                gui.setSplitPane2((gui.getSize().width *3)/4);
                gui.setVisible(true);	
                gui.setAlwaysOnTop(true); 
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.exit(1);
            } else {
                splash.setValue(pval++);
                Map userMap = (Map) db.DBselect("SELECT COUNT(ID) FROM SYNC.CONFIGS WHERE SYSTEMNAME = '"+hostname+"' and USERNAME = '"+userName+"'").get(0);
                int userCheck = Integer.valueOf(userMap.get("1").toString());
                if(userCheck == 0) createNewUser();
                gui.setSplitPane1(gui.getSize().width/4);
                gui.setSplitPane2((gui.getSize().width *3)/4);                
                ArrayList configList = db.DBselect("SELECT * FROM SYNC.CONFIGS WHERE SYSTEMNAME = '"+hostname+"' and USERNAME = '"+userName+"'");
                for(int i=0;i<configList.size();i++) {
                    configMap = (Map) configList.get(i);
                    int guiposX = Integer.valueOf(configMap.get("GUIPOS_X").toString());
                    int guiposY = Integer.valueOf(configMap.get("GUIPOS_Y").toString());
                    int guidimW = Integer.valueOf(configMap.get("GUIDIM_W").toString());
                    int guidimH = Integer.valueOf(configMap.get("GUIDIM_H").toString());
                    int deleteOpt = Integer.valueOf(configMap.get("CONFIRM_DELETE").toString());
                    gui.setConfirmDelete(deleteOpt == 1 ? true : false);
                    int syncOpt = Integer.valueOf(configMap.get("CONFIRM_SYNC").toString());
                    gui.setConfirmSync(syncOpt == 1 ? true : false);
                    int alwaysontopOpt = Integer.valueOf(configMap.get("ALLWAYSONTOP").toString());
                    gui.setAllwaysOnTop(alwaysontopOpt == 1 ? true : false);
                    int loglinesOpt = Integer.valueOf(configMap.get("LOGLINES").toString());
                    gui.setLogLines(loglinesOpt);
                    int mintotrayOpt = Integer.valueOf(configMap.get("MINTOTRAY").toString());
                    gui.setMinToTray(mintotrayOpt == 1 ? true : false);
                    if(mintotrayOpt != 1) gui.setDefaultCloseOperation(GUI.EXIT_ON_CLOSE);
                    int startminimizedOpt = Integer.valueOf(configMap.get("STARTMIN").toString());
                    gui.setStartMinimized(startminimizedOpt == 1 ? true : false);                    
                    int frontonsyncOpt = Integer.valueOf(configMap.get("FRONTONSYNC").toString());
                    gui.setToFrontOnSync(frontonsyncOpt == 1 ? true : false);                    
                    gui.setLocation(guiposX, guiposY);
                    gui.setSize(guidimW, guidimH);
                    optionMap = gui.getOptionMap();
                }
                if(splash != null) splash.killsplash();
                if(!(Boolean)optionMap.get("StartMinimized")) {
                    gui.setVisible(true);	
                    gui.setAlwaysOnTop(true); 
                } else {
                    if(!(Boolean)optionMap.get("MinToTray") || !gui.systemTraySupported()) {
                        if(!gui.systemTraySupported()) gui.setExtendedState(JFrame.ICONIFIED);
                    }
                }
                gui.setAlwaysOnTop((Boolean)optionMap.get("AlwaysOnTop"));
                sql = "SELECT * FROM SYNC.LOG WHERE USERNAME = '"+userName+"' ORDER BY TIMESTAMP DESC";
                ArrayList logArray = getDB().DBselect(sql);
                for (int i=0;i<logArray.size();i++) {
                    Map logMap = (Map) logArray.get(i);
                    String logEntry = logMap.get("ENTRY").toString();
                    String timestamp = logMap.get("TIMESTAMP").toString();
                    int style = Integer.valueOf(logMap.get("STYLE").toString());
                    gui.insertLog(timestamp.split("\\.")[0],3,null);
                    gui.insertLog("\t"+logEntry+"\n", style, null);                    
                }             
                writeLog("Database connected.", 1);   
                Map fileMap = (Map) db.DBselect("SELECT COUNT(*) FROM SYNC.FILES WHERE USERNAME = '"+userName+"'").get(0);
                int filecount = Integer.valueOf(fileMap.get("1").toString());
                if(filecount > 0) {
                    updateFileData();
                    gui.createTableModel(fileData);
		    gui.updateTableStatus();
		} else {
		    gui.createTableModel(null);
                    gui.addSyncItems();
		    updateFileData();
		    gui.createTableModel(fileData);
		}
		gui.createTablePopupMenu();
                gui.setMsgArea(PROGNAME+" v"+VERSION, Color.BLACK, Color.WHITE,7);
                
            }
        } catch (BadLocationException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String[] args) throws Exception {
        Main iSynx = new Main();
    }
       
    public boolean checkSyncToDirectories(File file) {
        boolean status = true;
        String[] paths = file.getAbsolutePath().split("\\"+File.separator);
        String runningPath = "";
        int i=0;   
        for(String path:paths) {
            runningPath += path;
            File tmpfile = new File(runningPath);
            if(!tmpfile.exists()) {
                if(i==0) return(false);
                status = tmpfile.mkdir();
            }
            runningPath += File.separator;
            i++;
        }  
        return(status);
    }
    public void cleanLog() {
        long mark = Calendar.getInstance().getTimeInMillis() - (86400000 * (Integer)optionMap.get("LogLength"));
        sql = "DELETE FROM SYNC.LOG WHERE TIMESTAMP < '"+getTimeStamp(mark)+"'";
        db.DBstatement(sql);
    }
    public boolean createNewUser() {
        Map idMap = (Map) db.DBselect("SELECT MAX(ID) FROM SYNC.CONFIGS").get(0);
        int id = Integer.valueOf(idMap.get("1").toString()) + 1;
        sql = "INSERT INTO SYNC.CONFIGS VALUES ("+
              id+","+
              getGUI().getWinCoord().x+","+
              getGUI().getWinCoord().y+","+
              getGUI().getWinSize().height+","+
              getGUI().getWinSize().width+","+
              "'"+hostname+"',"+
              "'"+userName+"',"+
              "'0.0.290.290.60',"+
              "1,"+
              "1,"+
              "1,"+
              "0,"+
              "0,"+
              "0,"+
              "30"+
              ")";
	if(db.DBstatement(sql)) {
	    sql = "INSERT INTO SYNC.LOG VALUES (" + 10031968 + "," + "'" + "User "+userName+" Created." + "'," + 1 + ",'" +userName+ "','" + getTimeStamp().split("\\.")[0] + "')";
	    return(db.DBstatement(sql));
	}
	return(false);
    }
    public void dbMaint() {
        writeLog("Performing DB Maintenance", 1);
        cleanLog();
        db.DBcompress("SYNC.FILES");
        db.DBcompress("SYNC.LOG");
        writeLog("DB Maintenance Complete", 1);
        writeLog("Closing Database", 1);        
    }
    public void exitApp() {        
	gui.setVisible(false);
        gui.systemTray(false);
	if(db.DBconnect()) {
	    sql = "UPDATE SYNC.CONFIGS SET "+
		  "GUIPOS_X = "+gui.getWinCoord().x+", "+
		  "GUIPOS_Y = "+gui.getWinCoord().y+", "+
		  "GUIDIM_H = "+gui.getWinSize().height+", "+
		  "GUIDIM_W = "+gui.getWinSize().width+", "+
		  "TABLE_COLS = '"+gui.getColumnWidth()+"' "+
		  "WHERE SYSTEMNAME = '"+hostname+"' AND USERNAME = '"+userName+"'";
	    db.DBstatement(sql);
            dbMaint();
            db.DBclose();
	}
	System.exit(0);
    }
    public boolean fileCopy (File fromFile, File toFile) {
        boolean status = false;
        JFrame cframe = new JFrame();
        gui.setAlwaysOnTop(false);
        cframe.setLocation((Toolkit.getDefaultToolkit().getScreenSize().height/2), Toolkit.getDefaultToolkit().getScreenSize().width/2);
        cframe.setVisible(true);
        cframe.setAlwaysOnTop(true);
        cframe.toFront();
        cframe.setVisible(false);
        int confirm = 0;
        if((Boolean)optionMap.get("ConfirmSync")) {
            confirm = JOptionPane.showConfirmDialog(
                cframe,
                "\t\t"+PROGNAME+"\n\n"+
                "Would you like to synchronize the following files?\n\n"+
                "\tPath: "+toFile.getParent()+"\n"+
                "\tFile: "+toFile.getName()+"\n"+
                "\tLast Modified: "+getTimeStamp(toFile.lastModified()).split("\\.")[0]+"\n"+
                "\tSize: "+fileSize(toFile)+"MB"+"\n\n"+
                "\t\twith\n\n"+
                "\tPath: "+fromFile.getParent()+"\n"+
                "\tFile: "+fromFile.getName()+"\n"+
                "\tLast Modified: "+getTimeStamp(fromFile.lastModified()).split("\\.")[0]+"\n"+
                "\tSize: "+fileSize(fromFile)+"MB"+"\n\n",
                "Confirm Sync",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE);
        }
        cframe.dispose();
        if(confirm == JOptionPane.YES_OPTION) {
            try {
                boolean read = fromFile.canRead();
                if(!read) fromFile.setReadable(true);
                FileInputStream fis = new FileInputStream(fromFile);
                FileOutputStream fos = new FileOutputStream(toFile);
                byte[] buf = new byte[1024];
                int i = 0;
                while ((i = fis.read(buf)) != -1) {
                fos.write(buf, 0, i);
                }
                fos.close();
                fis.close();
                if(!read) fromFile.setReadable(read);
                if (toFile.exists()) {
                    long modtime = (fromFile.lastModified() / 1000);
                    toFile.setLastModified(modtime * 1000);
                    toFile.setReadable(fromFile.canRead());
                    toFile.setWritable(fromFile.canWrite());
                    toFile.setExecutable(fromFile.canExecute());
                    fromFile.setLastModified(modtime * 1000);
                    status = true;
                    writeLog("SYNCRONIZED", 7);
                    writeLog("From:\t"+fromFile.getAbsolutePath(),15);
                    writeLog("To:\t"+toFile.getAbsolutePath(),15);                    
                }
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                status = false;
            }
        } else {
            status = false;
        }
        gui.setAlwaysOnTop((Boolean)optionMap.get("AlwaysOnTop")); 
        return status;
    }
    public boolean fileDelete(File file) {
	boolean status = true;
        JFrame dframe = new JFrame(); 
        gui.setAlwaysOnTop(false);
        dframe.setVisible(true);
        dframe.setAlwaysOnTop(true);  
        dframe.toFront();
        int confirm = 1;
        if((Boolean)optionMap.get("ConfirmDelete")) {
            confirm = JOptionPane.showConfirmDialog(
                dframe,
                "\t\t"+PROGNAME+"\n\n"+
                "Please confirm the delete action for the following file.\n\n"+
                "\tPath: "+file.getParent()+"\n"+
                "\tFile: "+file.getName()+"\n"+
                "\tLast Modified: "+getTimeStamp(file.lastModified()).split("\\.")[0]+"\n"+
                "\tSize: "+fileSize(file)+"MB"+"\n\n",
                "Confirm Delete",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE);
        }
        if(confirm == 0) {
            if (file.isDirectory()) {
                String[] children = file.list();
                for (int i=0; i<children.length; i++) {
                    status = fileDelete(new File(file, children[i]));
                    if(!status) break;
                }
            }
            if(status) {
                status = file.delete();
                writeLog("DELETED", 11);
                writeLog(file.getAbsolutePath(), 12);
            }
        }
        gui.setAlwaysOnTop((Boolean)optionMap.get("AlwaysOnTop")); 
        return(status);
        
    }   
    public String filePermisions(File file) {
        String perms = null;
        if(file.exists()) {
            perms = file.isDirectory() == true ? "d" : "f";
            if(file.isHidden()) perms = "h";
            perms += file.canRead() == true ? "r" : "-";
            perms += file.canWrite() == true ? "w" : "-";
            perms += file.canExecute() == true ? "x" : "-";
        } else {
            perms = "----";
        }
        return(perms);
    }
    public File fileRoot(File file) {
        String rootpath = file.getAbsolutePath().split("\\"+File.separator)[0]+File.separator;
        File root = new File(rootpath);
        return(root);
    }
    public float fileSize(File file) {
	float size = 0;
	if(file.isFile()) {
	    size = ((float)file.length()/(float)1000000);
	} else if(file.isDirectory()) {
	    float tsize = 0;
	    File[] flist = file.listFiles();
	    for (int i=0;i < flist.length;i++) {
		File tfile = new File(flist[i].getAbsolutePath());
		if(tfile.isFile()) tsize+=((float)tfile.length()/(float)1000000);
		else { tsize+=fileSize(tfile); }
	    }
            size = tsize;
	}
	return(size);
    }
    public void fileUNZIP(File zipfile, File zipto) {
/*
public class UnZip {
   public static void main (String argv[]) {
      try {
         final int BUFFER = 2048;
         BufferedOutputStream dest = null;
         FileInputStream fis = new 
	   FileInputStream(argv[0]);
         CheckedInputStream checksum = new 
           CheckedInputStream(fis, new Adler32());
         ZipInputStream zis = new 
           ZipInputStream(new 
             BufferedInputStream(checksum));
         ZipEntry entry;
         while((entry = zis.getNextEntry()) != null) {
            System.out.println("Extracting: " +entry);
            int count;
            byte data[] = new byte[BUFFER];
            // write the files to the disk
            FileOutputStream fos = new 
              FileOutputStream(entry.getName());
            dest = new BufferedOutputStream(fos, 
              BUFFER);
            while ((count = zis.read(data, 0, 
              BUFFER)) != -1) {
               dest.write(data, 0, count);
            }
            dest.flush();
            dest.close();
         }
         zis.close();
         System.out.println("Checksum: 
           "+checksum.getChecksum().getValue());
      } catch(Exception e) {
         e.printStackTrace();
      }
   }
}
*/         
    }
    public void fileZIP(File[] files, File zipfile) {
        FileOutputStream zipstream = null;
        try {
            final int BUFFER = 2048;
            zipstream = new FileOutputStream(zipfile.getAbsolutePath());
            ZipOutputStream outstream = new ZipOutputStream(new BufferedOutputStream(zipstream));
            byte[] data = new byte[BUFFER];
            for (int i = 0; i < files.length; i++) {
                FileInputStream instream = new FileInputStream(files[i].getAbsolutePath());
                BufferedInputStream buffedIN = new BufferedInputStream(instream, BUFFER);
                ZipEntry zipEntry = new ZipEntry(files[i].getAbsolutePath());
                outstream.putNextEntry(zipEntry);
                int count;
                while ((count = buffedIN.read(data, 0, BUFFER)) != -1) {
                    outstream.write(data, 0, count);
                }
                buffedIN.close();
            }
            outstream.close();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public boolean hasLicense() {
        return(license.getLicense());
    }       
    private Thread RootMonitor() {
        Thread thread = new Thread("RootMonitor") {
            @Override
            @SuppressWarnings("static-access")
            public void run() {
                updateRootMap();
                while(true){
		    try {
                        File[] newroots = fc.getFileSystemView().getRoots()[0].listRoots();
                        if (!roots.equals(newroots)) {
                            int lrc = newroots.length;
                            if(roots.length > lrc) {
                                for(File root:roots) {
                                    boolean found = false;
                                    for(File newroot: newroots){
                                        if(root.equals(newroot)){
                                            found = true;
                                            break;
                                        }
                                    }
                                    if(!found) {
                                        String media = rootMap.get(root.getAbsolutePath().replace("\\", ""));
                                        if(media != null) {
                                            String mediatype = media.split("\\|\\|")[0];
                                            String medianame = media.split("\\|\\|")[1];
                                            gui.setMsgArea(mediatype+":("+root.getAbsolutePath().replace("\\", "")+") \""+medianame.replace("\\'", "\\'||char(39)||\\'")+")"+"\tEJECTED.", Color.BLUE.darker(), Color.WHITE,7);
                                            writeLog(mediatype+":("+root.getAbsolutePath().replace("\\", "")+") \""+medianame.replace("'", "'")+"\""+"\tEJECTED.", 7);
                                        }
                                        gui.updateTableStatus();
                                        break;
                                    }
                                }
                                roots = newroots;
                            } else if(roots.length < lrc) {
                                for(File newroot:newroots) {
                                    boolean found = false;
                                    for(File root: roots){
                                        if(newroot.equals(root)){
                                            found = true;
                                            break;
                                        }
                                    }
                                    if(!found) {
                                        String mediatype = fc.getFileSystemView().getSystemTypeDescription(newroot);
                                        String medianame = fc.getFileSystemView().getSystemDisplayName(newroot).split("\\(")[0].trim();
                                        gui.setMsgArea(mediatype+":("+newroot.getAbsolutePath().replace("\\", "")+") \""+medianame+"\"\tDETECTED.", Color.GREEN.darker(),Color.WHITE,7);
                                        writeLog(mediatype+":("+newroot.getAbsolutePath().replace("\\", "")+") \""+medianame+"\"\tDETECTED.", 15);
                                        updateRootMap();
                                        gui.updateTableStatus();
                                        break;
                                    }
                                }
                                roots = newroots;
                            }
                        }
                        Thread.sleep(3000);
                    } catch (InterruptedException ex) {
                    }
                }
            }
        };
        return(thread);
    } 
    public boolean synchronizeItem(int mapID, int tableRow) {
        boolean status = true;
        Map dataMap = new HashMap();
        for(int i=0;i<fileData.size();i++) {
            dataMap = (Map)fileData.get(i);
                if(Integer.valueOf(dataMap.get("ID").toString()) == mapID) {
                File syncfile = new File(dataMap.get("PATH").toString());
                File syncto = new File(dataMap.get("SYNCTO").toString());
                File synctoFILE = new File(syncto.getAbsolutePath()+File.separator+syncfile.getName());
                //Neither Exists, RETURN
                if(!syncfile.exists() && !syncto.exists()) return(false);
                if(checkSyncToDirectories(syncto)) {
                    //Both Exists, Update with GREATEST lastModified
                    if(syncfile.exists() && synctoFILE.exists()) {
                        if(syncfile.lastModified() > synctoFILE.lastModified()) {
                            fileCopy(syncfile, synctoFILE);
                            gui.repaintSyncTable();
                        } else
                        if(synctoFILE.lastModified() > syncfile.lastModified()) {
                            fileCopy(synctoFILE, syncfile);
                            gui.repaintSyncTable();
                        }
                    } else
                    //SyncFile EXISTS ONLY, copy to SYNCTO 
                    if(syncfile.exists() && ! synctoFILE.exists()) {
                        long DBlastModified = getTimeEpoch(dataMap.get("TIMESTAMP").toString());
                        if(syncfile.lastModified() != DBlastModified) {
                            fileCopy(syncfile, synctoFILE);
                            gui.repaintSyncTable();
                        } else {
                            //If lastModify timestamp == DBLastModify, items had been sync'ed (Deleted)
                            fileDelete(syncfile);
                            sql = "DELETE FROM SYNC.FILES WHERE ID = "+mapID;
                            db.DBstatement(sql);
                            gui.removeSyncTableRow(tableRow);
                        }
                    } else 
                    //SynctoFile EXISTS ONLY, copy to SYNC
                    if(synctoFILE.exists() && !syncfile.exists()) {
                        long DBlastModified = getTimeEpoch(dataMap.get("TIMESTAMP").toString());
                        if(synctoFILE.lastModified() != DBlastModified) {
                            fileCopy(synctoFILE, syncfile);
                            gui.repaintSyncTable();
                        } else {
                            //If lastModify timestamp == DBLastModify, items had been sync'ed (Deleted)                            
                            fileDelete(synctoFILE);
                            sql = "DELETE FROM SYNC.FILES WHERE ID = "+mapID;
                            db.DBstatement(sql);
                            gui.removeSyncTableRow(tableRow);
                        }
                    }    
                }
                if(status){
                    sql = "UPDATE SYNC.FILES SET "+
                          "TIMESTAMP = '"+getTimeStamp(syncfile.lastModified())+"' "+
                          "WHERE ID = "+mapID;
                    db.DBstatement(sql);
                }
		break;
            }
        }
        return(status);
    }
    public ArrayList updateFileData() {
        sql = "SELECT * FROM SYNC.FILES WHERE USERNAME = '"+getUserName()+"'";
        fileData = db.DBselect(sql);
        return(fileData);
    }
    private void updateRootMap() {
        rootMap = new HashMap<String,String>();
        for(File root:roots) {
            if(fc.getFileSystemView().getSystemTypeDescription(root) != null) {
                String mediatype = fc.getFileSystemView().getSystemTypeDescription(root);
                String medianame = fc.getFileSystemView().getSystemDisplayName(root);
                if(medianame.length() > 0) {
                    medianame = medianame.split("\\(")[0].trim();
                    rootMap.put(root.getAbsolutePath().replace("\\", ""), mediatype+"||"+medianame);
                }
            }
        }        
    }
    public void writeLog(String text, int style) {
        try {
            if((Integer)optionMap.get("LogLength") > 0) {
                sql = "SELECT MAX(ID) FROM SYNC.LOG";
                Map map = (Map) db.DBselect(sql).get(0);
                int logID = Integer.valueOf(map.get("1").toString()) + 1;
                sql = "INSERT INTO SYNC.LOG VALUES (" + logID + "," + "'" + text.replace("'", "''") + "'," + style + ",'" +userName+ "','" + getTimeStamp().split("\\.")[0] + "')";
                db.DBstatement(sql);
            }
            Icon icon = null;
            text = text.replace("\'\'", "\'");
            gui.insertLog(getTimeStamp().split("\\.")[0],3,null);
            gui.insertLog("\t"+text+"\n", style, icon);
        } catch (BadLocationException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Map getConfigMap() {
	return(configMap);
    }
    public DB getDB() {
        return(db);
    }
    public JFileChooser getFileChooser() {
        return(fc);
    }    
    public float getFileSize(File file) {
	float size = 0;
	if(file.isFile()) {
	    size = ((float)file.length()/(float)1000000);
	} else if(file.isDirectory()) {
	    float tsize = 0;
	    File[] flist = file.listFiles();
	    for (int i=0;i < flist.length;i++) {
		File tfile = new File(flist[i].getAbsolutePath());
		if(tfile.isFile()) tsize+=((float)tfile.length()/(float)1000000);
		else { tsize+=getFileSize(tfile); }
	    }
            size = tsize;
	}
	return(size);
    }    
    public GUI getGUI() {
        return(gui);
    }
    public Image getGUIIcon() {
	Image winimage = new ImageIcon(getURL(appIconRef)).getImage();
//
	return(winimage);
    }
    public String getHostName() {
	hostname = null;
		try {
			InetAddress addr = InetAddress.getLocalHost();
			hostname = addr.getHostName();
		} catch (UnknownHostException ex) {
			ex.printStackTrace();
		}
        return(hostname);
    }
    public Map getOptionMap() {
        optionMap = gui.getOptionMap();
        return(optionMap);
    }   
    protected String getOSName() {
	String osName = System.getProperty("os.name");
	return(osName);
    }    
    public int getRecursiveFileCount(File file) {
	int count=0;
	if (file.isDirectory()) {
	    String[] children = file.list();
            for (int i=0;i<children.length;i++) {
                count+=getRecursiveFileCount(new File(file, children[i]));
	    }
	} else if (file.isFile()) count++;
	return(count);
    }
    public long getTimeEpoch() {
        return(Calendar.getInstance().getTimeInMillis());
    }
    public long getTimeEpoch(String datetime) {
        try {
            long epoch = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datetime).getTime();
            return epoch;
        } catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            return(-1);
        }
    }
    public String getTimeStamp() {
        long epoch = Calendar.getInstance().getTimeInMillis();
        String timestamp = new Timestamp(epoch).toString();
        return(timestamp);
    }
    public String getTimeStamp(long epoch) {
        String timestamp = new Timestamp(epoch).toString();
        return(timestamp);
    }
    public URL getURL(String link) {
        String UniRefLink = null;
        JarFile mainjar = null;
        URL refURL = null;
        try {
            mainjar = new JarFile(PROGNAME+".jar");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Enumeration entries = mainjar.entries();
        while (entries.hasMoreElements()) {
            String curURL = entries.nextElement().toString();
            if(curURL.contains(link)) {
            UniRefLink = curURL;
            break;
            }
        }
            try {
                String curDir = System.getProperty("user.dir");
                refURL = new URL("jar:file:/"+curDir+File.separatorChar+PROGNAME+".jar!/"+UniRefLink);
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
            }
        return(refURL);
    }       
    public String getUserName() {
	String osUser = System.getProperty("user.name");
	return(osUser);
    }
}
