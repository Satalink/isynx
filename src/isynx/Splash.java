/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package isynx;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

/**
 *
 * @author Satalink
 */
public class Splash extends JPanel {
    

    private int winX;
    private int winY;
    private Pkg_Interface main;
    private String splashRef = "Synx.png";
    private JWindow f;
    private JProgressBar p;
    private boolean alive = true;

    public Splash(Pkg_Interface iface){
	main = iface;
        f = new JWindow();
        p = new JProgressBar();
        p.setSize(256, 24);
        p.setVisible(false);
        f.setSize(256,256);
        Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize() ;
        winX = (int)((screenSize.getWidth()/2)-50);
        winY = (int)((screenSize.getHeight()/2)-50);
        f.setLocation( winX, winY );
        f.getContentPane().setLayout(new BorderLayout()); 
        f.getContentPane().add(this, BorderLayout.CENTER);
        f.add(p, -1);
        f.setVisible(true);        
        paintComponent(f.getGraphics());
        Thread splashthread = splashThread();
        splashthread.setPriority(Thread.MIN_PRIORITY);
        splashthread.start();
    }
    protected Thread splashThread(){
        Thread thread = new Thread("SplashThread") {
            @Override
            @SuppressWarnings("static-access")
            public void run() {
                try {
                    while (alive) {
                        if(f.isAlwaysOnTopSupported() && !f.isAlwaysOnTop()) {
                            f.setAlwaysOnTop(true);
                        }
                        Thread.sleep(10);
                    }
                } catch (InterruptedException ex) {
                    Logger.getLogger(Splash.class.getName()).log(Level.SEVERE, null, ex);
                }
                f.setVisible(false);
            }
        };
        return(thread);
    }    
    public void paintComponent(Graphics g) {
        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        Image splashimage = null;
        Image thumbnail = null;
        try {
            splashimage = ImageIO.read(main.getURL(splashRef));
            thumbnail = splashimage.getScaledInstance(f.getWidth(),f.getHeight(), Image.SCALE_AREA_AVERAGING);
            g.drawImage(thumbnail,0,0,null);
        } catch (IOException ex) {
            Logger.getLogger(Splash.class.getName()).log(Level.SEVERE, null, ex);
            f.dispose();
        }
    }
    public void killsplash() {
        alive = false;
        f.setVisible(false);
        f.dispose();
    }
    public void setMax(int progressmax) {
        p.setMaximum(progressmax);
    }
    public void setMin(int progressmin) {
        p.setMinimum(progressmin);
    }
    public void setValue(int progressvalue) {
        p.setValue(progressvalue);
    }
}

